package exercises1;

import utils.ScannerUtils;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        System.out.println("Introduceti un numar: ");
        int number = ScannerUtils.readIntFromUser();
        int sumOfDigits = 0;
        while (number >0) {
            sumOfDigits = sumOfDigits + (number%10);
            number = number/10;
        }
        System.out.println( sumOfDigits);
    }
}
