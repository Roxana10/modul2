package exercises1;

import utils.ScannerUtils;

public class Task9 {
    public static void main(String[] args) {

        System.out.println("Introduceti numarul de valuri");
        int numberOfWaves = ScannerUtils.readIntFromUser();
        System.out.println("Introduceti numarul de linii");
        int numberOfLines = ScannerUtils.readIntFromUser();

        String star = "*";
        String space = "";

        for (int i = 1; i <= numberOfLines; i++) {
            int numberOfSpacesBetweenStars = (2 * numberOfLines) - (2 * i);
            String line = "%" + i + "s%" + numberOfSpacesBetweenStars + "s%-" + i + "s"; // pt i =4; " %4s%0s%-4s"
            if (numberOfSpacesBetweenStars == 0) {
                line = "%" + i + "s%s%-" + i + "s";
            }
            for (int j = 1; j <= numberOfWaves; j++) {
                System.out.printf(line, star, space, star);
            }

            System.out.println();

        }


    }
}
