package exercises1;

import utils.ScannerUtils;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {

        //sa calculam perimetrul unui cerc citind de la tastatura diametrul unui cerc.

        Scanner tastatura = new Scanner (System.in);
        System.out.println( " Introduceti diametrul cercului, va rog. ");
        float diameter = ScannerUtils.readFloatFromUser();
        final double PI = Math.PI;

        double perimetru = PI * diameter;
        System.out.println( "perimetrul este :" + perimetru);

    }
}
