package exercises1;

import utils.ScannerUtils;

public class Task7 {
    public static void main(String[] args) {

        System.out.println( " Scrieti un numar de la tastatura");
        int number = ScannerUtils.readIntFromUser();

        if (number == 0 || number == 1) {
            System.out.println(1);
        }

        int fibonacciNumber=0;
        int previousNumber =1;
        int lastNumber = 1;

        for (int i =2; i<number; i++) {
            fibonacciNumber = previousNumber + lastNumber;
            previousNumber = lastNumber;
            lastNumber = fibonacciNumber;

        }

        System.out.println( "Numarul lui Fibonnacci pentru valoarea introdusa " + number + " este: "  + fibonacciNumber);








    }
}
