package exercises1;

import utils.ScannerUtils;

public class Task6 {
    public static void main(String[] args) {

        System.out.println("Introduceti un numar n: ");
        int n = ScannerUtils.readIntFromUser();
        double harmonicSum = 0;
        for (int i= 1; i<=n; i++) {
            harmonicSum = harmonicSum + (1.0/i);
        }
        System.out.printf( "Valoarea sumei armonice este: %.3f  " , harmonicSum);
    }
}
