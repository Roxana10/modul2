package exercises1;

import utils.ScannerUtils;

public class Task8 {
    public static void main(String[] args) {
        System.out.println("Introduceti primul numar: ");
        float firstNr = ScannerUtils.readFloatFromUser();
        ScannerUtils.getScanner().nextLine();
        System.out.println(" introduceti operatia matematica ( +; -; /; *;)") ;
        char opSymbol = ScannerUtils.getScanner().nextLine().charAt(0);

        System.out.println("Introduceti al 2-lea numar: ");
        float secondNr = ScannerUtils.readFloatFromUser();

        float result = 0;
        switch (opSymbol) {
            case '+':
                result = firstNr + secondNr;
                break;
            case '-':
                result = firstNr - secondNr;
                break;
            case '*':
                result = firstNr * secondNr;
                break;
            case '/':
                if (secondNr != 0) {
                    result = firstNr / secondNr;
                } else {
                    System.out.println("Cannot calculate");
                }
                break;
            default:
                System.out.println("Invalid symbol ");

        }
        System.out.println( result );


    }
}
