package exercises1;

import utils.ScannerUtils;


public class Task3 {
    public static void main(String[] args) {

        int a, b, c;
        System.out.println( " Va rog introduceti valoarea lui a ");
        a = ScannerUtils.readIntFromUser();
        System.out.println( " Va rog introduceti valoarea lui b ");
        b = ScannerUtils.readIntFromUser();
        System.out.println( " Va rog introduceti valoarea lui c ");
        c = ScannerUtils.readIntFromUser();

        int delta = (b*b) - (4*a*c);

        if (delta < 0 ) {
            System.out.println( " Delta engativ ");
            return;
        }

        double x1 =(( -b - Math.sqrt(delta))) / (2*a);
        double x2 =(( -b + Math.sqrt(delta))) / (2*a);
        System.out.printf( " x1 = %.2f  x2= %.2f "  , x1 , x2);



    }
}
